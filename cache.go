package main

import (
	"fmt"
	"github.com/patrickmn/go-cache"
	"time"
)

type Employee struct {
	Name    string
	Address string
}

func main() {
	newEmployee := &Employee{
		Name:    "Tharun",
		Address: "Tirupati",
	}
	c := cache.New(3*time.Minute, 5*time.Minute)
	abcd, found := c.Get("abcd")

	if found {
		a := abcd.([]Employee)
		a = append(a, *newEmployee)
		c.Set("abcd", a, -1)
		fmt.Println("Stored from if part")
	} else {
		c.Set("abcd", *newEmployee, -1)
		fmt.Println("Stored from else part")
	}
	abcd, found = c.Get("abcd")
	if found {
		fmt.Println(abcd)
	}
}
